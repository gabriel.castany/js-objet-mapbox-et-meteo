export class DummyControl {

    map;
    container;


    onAdd(map) {
        this.map = map;
        this.container = document.createElement('div');
        this.container.className = 'mapboxgl-ctrl';
        this.container.textContent = 'Hello, world';
        this.container.addEventListener('click', this.handlerDummyClick.bind(this));
        return this.container;
        }
         
        onRemove() {
            this.container.removeEventListener(this.handlerDummyClick);
            this.container.remove();
            this.container = undefined;
            this.map = undefined;
            }

        handlerDummyClick() {
            console.log(this.container.textContent);
        }
}